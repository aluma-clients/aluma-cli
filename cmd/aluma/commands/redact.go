package commands

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/services"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gitlab.com/aluma-clients/aluma-cli/output/progress"
	"gitlab.com/aluma-clients/aluma-cli/output/resultsWriters"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

type redactWithExtractorArgs struct {
	extractorName string
	filePatterns  []string
	readProfile   string
}

type RedactWithExtractorCmd struct {
	ExtractorName    string
	FilePaths        []string
	ReadProfileArgs  client.ReadProfileArgs
	RedactionService RedactWithExtractorService
}

//go:generate mockery -name "RedactWithExtractorService"
type RedactWithExtractorService interface {
	RedactAllWithExtractor(ctx context.Context, files []string, extractorName string, args client.ReadProfileArgs) error
}

func ConfigureRedactWithExtractionCommand(ctx context.Context,
	app *kingpin.Application,
	globalFlags *config.GlobalFlags) {
	args := &redactWithExtractorArgs{}
	cmd := &RedactWithExtractorCmd{}
	redactCli := app.
		Command("redact", "Perform data redaction on a file or set of files.")

	redactWithExtractorCli := redactCli.Command("with-extractor",
		"Use fields from an extractor to define areas to redact. ").
		Action(func(parseContext *kingpin.ParseContext) error {
			err := cmd.initWithArgs(args, globalFlags)
			if err != nil {
				return err
			}
			return cmd.Execute(ctx)
		})

	redactWithExtractorCli.Arg("extractor-name", "The name of the extractor to use.").
		Required().
		StringVar(&args.extractorName)

	redactWithExtractorCli.Arg("files", "The files to read.").
		Required().
		StringsVar(&args.filePatterns)

	redactWithExtractorCli.Flag("read-profile", "The read profile to use [default: none (english language)].").
		Short('r').
		Default("").
		StringVar(&args.readProfile)

	addFileHandlingFlagsTo(globalFlags, redactCli)
}

func (cmd *RedactWithExtractorCmd) initWithArgs(args *redactWithExtractorArgs, flags *config.GlobalFlags) error {
	resultsWriter, err := resultsWriters.NewRedactResultsWriter(flags.MultiFileOut, flags.OutputFile)

	if err != nil {
		return err
	}

	progressHandler := progress.NewProgressHandler(resultsWriter,
		flags.ShowProgress, os.Stderr)

	cmd.FilePaths, err = GlobMany(args.filePatterns)

	if err != nil {
		return err
	}

	readProfile := args.readProfile
	if readProfile != "" {
		cmd.ReadProfileArgs = client.ReadProfileArgs{
			UseReadProfile: true,
			ProfileName:    readProfile,
		}
	} else {
		cmd.ReadProfileArgs = client.ReadProfileArgs{
			UseReadProfile: false,
		}
	}

	apiClient, err := initApiClient(flags.ClientId,
		flags.ClientSecret,
		flags.LogHttp)

	if err != nil {
		return err
	}

	if !config.IsOutputRedirected() &&
		!flags.IsOutputSpecified() {
		return errors.New("you must use '-o' or '-m' or redirect stdout when redacting files")
	}

	fileRedactor := client.NewFileRedactor(apiClient.Documents, apiClient.Documents,
		apiClient.Documents,
		apiClient.Documents)

	cmd.RedactionService = services.NewParallelRedactionService(fileRedactor, apiClient.Documents, progressHandler)
	cmd.ExtractorName = args.extractorName

	return nil
}

// ExecuteRedact is the main entry point for the 'redact' command.
func (cmd *RedactWithExtractorCmd) Execute(ctx context.Context) error {
	err := cmd.RedactionService.RedactAllWithExtractor(ctx, cmd.FilePaths, cmd.ExtractorName, cmd.ReadProfileArgs)

	return errors.Wrap(err, "redaction failed")
}
