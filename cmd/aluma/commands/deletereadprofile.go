package commands

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gopkg.in/alecthomas/kingpin.v2"
)

type DeleteReadProfileCmd struct {
	Client          client.ReadProfileGetterDeleter
	ReadProfileName string
}

type deleteReadProfileArgs struct {
	ReadProfileName string
}

func ConfigureDeleteReadProfileCmd(ctx context.Context, deleteCmd *kingpin.CmdClause,
	flags *config.GlobalFlags) {
	args := &deleteReadProfileArgs{}
	deleteReadProfileCmd := &DeleteReadProfileCmd{}

	deleteReadProfileCli := deleteCmd.Command("read-profile", "Delete aluma read profile.").
		Action(func(parseContext *kingpin.ParseContext) error {
			return ExecuteWithMessage(fmt.Sprintf("Deleting read profile '%s'... ",
				args.ReadProfileName),
				func() error {
					err := deleteReadProfileCmd.initFromArgs(args, flags)
					if err != nil {
						return err
					}
					return deleteReadProfileCmd.Execute(ctx)
				})
		})

	deleteReadProfileCli.
		Arg("name", "The name of the read profile to delete.").
		Required().
		StringVar(&args.ReadProfileName)
}

// Execute runs the 'delete ReadProfile' command.
func (cmd *DeleteReadProfileCmd) Execute(ctx context.Context) error {
	profiles, err := cmd.Client.GetAll(ctx)

	if err != nil {
		return err
	}

	if !profiles.Contains(cmd.ReadProfileName) {
		return errors.New("there is no read profile named '" + cmd.ReadProfileName + "'")
	}

	return cmd.Client.Delete(ctx, cmd.ReadProfileName)
}

func (cmd *DeleteReadProfileCmd) initFromArgs(args *deleteReadProfileArgs,
	flags *config.GlobalFlags) error {
	cmd.ReadProfileName = args.ReadProfileName

	client, err := initApiClient(flags.ClientId, flags.ClientSecret, flags.LogHttp)

	if err != nil {
		return err
	}

	cmd.Client = client.ReadProfiles
	return nil
}
