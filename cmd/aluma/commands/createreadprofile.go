package commands

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gopkg.in/alecthomas/kingpin.v2"
)

type CreateReadProfileCmd struct {
	Creator   client.ReadProfileCreator
	Name      string
	Languages []string
}

type createReadProfileArgs struct {
	name      string
	languages *[]string
}

func ConfigureCreateReadProfileCmd(ctx context.Context, createCmd *kingpin.CmdClause,
	flags *config.GlobalFlags) {
	args := &createReadProfileArgs{}
	CreateReadProfileCmd := &CreateReadProfileCmd{}

	createProfileCli := createCmd.Command("read-profile",
		"Create aluma read profile.").
		Action(func(parseContext *kingpin.ParseContext) error {
			return ExecuteWithMessage(fmt.Sprintf("Creating read profile '%s'...", args.name),
				func() error {

					err := CreateReadProfileCmd.initFromArgs(args, flags)

					if err != nil {
						return err
					}

					return CreateReadProfileCmd.Execute(ctx)
				})
		})

	createProfileCli.
		Arg("name", "The name of the new read profile.").
		Required().
		StringVar(&args.name)

	// This flag can be set more than once.
	args.languages = createProfileCli.Flag("language",
		"The three-letter code of the language to use for reading.").
		Short('l').
		Strings()
}

func (cmd *CreateReadProfileCmd) Execute(ctx context.Context) error {
	_, err := cmd.Creator.Create(ctx, cmd.Name, cmd.Languages)
	if err != nil {
		return err
	}

	return nil
}

func (cmd *CreateReadProfileCmd) initFromArgs(args *createReadProfileArgs, flags *config.GlobalFlags) error {
	if len(*args.languages) == 0 {
		//goland:noinspection GoErrorStringFormat
		return errors.New(
			"Please specify at least one language to use for reading using the -l or --language flag. " +
				"The value is the three-letter code for the language.")
	}

	client, err := initApiClient(flags.ClientId, flags.ClientSecret, flags.LogHttp)

	if err != nil {
		return err
	}

	cmd.Creator = client.ReadProfiles
	cmd.Name = args.name
	cmd.Languages = *args.languages
	return err
}
