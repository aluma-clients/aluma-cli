package tests

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands/mocks"
	"testing"
)

type ListModuleSuite struct {
	suite.Suite
	sut    *commands.ListModulesCmd
	client *mocks.ModuleGetter
	ctx    context.Context
}

func (suite *ListModuleSuite) SetupTest() {
	suite.client = new(mocks.ModuleGetter)

	suite.sut = &commands.ListModulesCmd{
		Client: suite.client,
	}
	suite.ctx = context.Background()
}

func TestListModuleSuiteRunner(t *testing.T) {
	suite.Run(t, new(ListModuleSuite))
}

func (suite *ListModuleSuite) TestGetAllModules_Execute_Calls_The_Client() {
	expectedModules := aListOfModules("charlie", "jo", "chris").(client.ModuleList)
	suite.client.On("GetAll", mock.Anything).Return(expectedModules, nil)

	suite.sut.Execute(suite.ctx)

	suite.client.AssertCalled(suite.T(), "GetAll", suite.ctx)
}

func (suite *ListModuleSuite) TestGetAllModules_Execute_Returns_An_Error_If_The_Modules_Cannot_Be_Retrieved() {
	expectedErr := errors.New("Failed")
	suite.client.On("GetAll", mock.Anything).Return(nil, expectedErr)

	actualErr := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), expectedErr, actualErr)
}

func aListOfModules(ids ...string) interface{} {
	expected := make(client.ModuleList, len(ids))

	for index, id := range ids {
		expected[index] = client.Module{
			Name: id,
			ID:   id,
		}
	}

	return expected
}
