package tests

import (
	"context"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands/mocks"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type readCommandSuite struct {
	suite.Suite
	readerService   *mocks.ReaderService
	sut             *commands.ReadCmd
	filePatterns    []string
	ctx             context.Context
	readMode        client.ReadMode
	expectedErr     error
	readProfileArgs client.ReadProfileArgs
}

func (suite *readCommandSuite) SetupTest() {
	suite.readerService = new(mocks.ReaderService)
	suite.ctx, _ = context.WithCancel(context.Background())
	suite.filePatterns = []string{generators.String("file1"), generators.String("file2")}

	suite.expectedErr = errors.New("simulated error")

	suite.readerService.
		On("ReadAll", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(suite.expectedErr)

	suite.readMode = client.ReadPDF
	suite.readProfileArgs = client.ReadProfileArgs{
		UseReadProfile: generators.Bool(),
		ProfileName:    generators.String("profile"),
	}

	suite.sut = &commands.ReadCmd{
		FilePaths:       suite.filePatterns,
		ReaderService:   suite.readerService,
		ReadMode:        suite.readMode,
		ReadProfileArgs: suite.readProfileArgs,
	}
}

func TestReadCommandSuiteRunner(t *testing.T) {
	suite.Run(t, new(readCommandSuite))
}

func (suite *readCommandSuite) Test_ReaderService_ReadAll_Called_With_Correct_Params() {
	_ = suite.sut.Execute(suite.ctx)

	suite.readerService.AssertCalled(suite.T(),
		"ReadAll",
		suite.ctx,
		suite.filePatterns,
		suite.readMode,
		suite.readProfileArgs)
}

func (suite *readCommandSuite) Test_Error_Returned_From_ReaderService() {
	actualErr := suite.sut.Execute(suite.ctx)

	assert.EqualError(suite.T(), errors.Cause(actualErr), suite.expectedErr.Error())
}
