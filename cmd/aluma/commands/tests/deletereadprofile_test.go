package tests

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/client/mocks"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"testing"
)

type DeleteReadProfileSuite struct {
	suite.Suite
	sut             *commands.DeleteReadProfileCmd
	client          *mocks.ReadProfileGetterDeleter
	ReadProfileName string
	ctx             context.Context
}

func (suite *DeleteReadProfileSuite) SetupTest() {
	suite.client = new(mocks.ReadProfileGetterDeleter)
	suite.client.On("GetAll", mock.Anything).Return(
		AListOfReadProfiles("charlie", "jo", "chris"), nil)

	suite.client.On("Delete", mock.Anything, mock.Anything).Return(nil)

	suite.ReadProfileName = "charlie"
	suite.sut = &commands.DeleteReadProfileCmd{
		Client:          suite.client,
		ReadProfileName: suite.ReadProfileName,
	}
	suite.ctx = context.Background()
}

func TestDeleteReadProfileSuiteRunner(t *testing.T) {
	suite.Run(t, new(DeleteReadProfileSuite))
}

func (suite *DeleteReadProfileSuite) TestDeleteReadProfile_Execute_Deletes_The_Named_ReadProfile_When_It_Exists() {
	suite.sut.Execute(suite.ctx)

	suite.client.AssertCalled(suite.T(), "GetAll", suite.ctx)
	suite.client.AssertCalled(suite.T(), "Delete", suite.ctx, suite.ReadProfileName)
}

func (suite *DeleteReadProfileSuite) TestDeleteReadProfile_Execute_Does_Not_Delete_The_Named_ReadProfile_When_It_Does_Not_Exist() {
	suite.sut.Execute(suite.ctx)

	suite.client.AssertCalled(suite.T(), "GetAll", suite.ctx)
	suite.client.AssertNotCalled(suite.T(), "Delete", suite.ctx)
}

func (suite *DeleteReadProfileSuite) TestDeleteReadProfile_Execute_Returns_An_Error_If_The_ReadProfiles_Cannot_Be_Retrieved() {
	suite.client.ExpectedCalls = nil
	expected := errors.New("Failed")
	suite.client.On("GetAll", mock.Anything, mock.Anything).Return(nil, expected)

	actual := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), expected, actual)
	suite.client.AssertNotCalled(suite.T(), "Delete")
}

func (suite *DeleteReadProfileSuite) TestDeleteReadProfile_Execute_Returns_An_Error_If_The_ReadProfile_Cannot_Be_Deleted() {
	suite.client.ExpectedCalls = nil
	suite.client.On("GetAll", mock.Anything, mock.Anything).Return(
		AListOfReadProfiles("charlie", "jo", "chris"), nil)

	expected := errors.New("Failed")
	suite.client.On("Delete", mock.Anything, mock.Anything).Return(expected)

	actual := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), expected, actual)
}

func AListOfReadProfiles(names ...string) interface{} {
	expected := make(client.ReadProfileList, len(names))

	for index, name := range names {
		expected[index] = client.ReadProfile{
			Name:      name,
			Languages: []string{"eng"},
		}
	}

	return expected
}
