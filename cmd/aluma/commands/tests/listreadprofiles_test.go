package tests

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	mocks2 "gitlab.com/aluma-clients/aluma-cli/client/mocks"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type ListReadProfilesSuite struct {
	suite.Suite
	sut    *commands.ListReadProfilesCmd
	client *mocks2.ReadProfileGetter
	ctx    context.Context
}

func (suite *ListReadProfilesSuite) SetupTest() {
	suite.client = new(mocks2.ReadProfileGetter)

	suite.sut = &commands.ListReadProfilesCmd{
		Client: suite.client,
	}
	suite.ctx = context.Background()
}

func TestListReadProfilesSuiteRunner(t *testing.T) {
	suite.Run(t, new(ListReadProfilesSuite))
}

func (suite *ListReadProfilesSuite) TestGetAllProfiles_Execute_Calls_The_Client() {
	expectedProfiles := aListOfProfiles("profile1", "profile2", "profile3")
	suite.client.On("GetAll", mock.Anything).Return(expectedProfiles, nil)

	suite.sut.Execute(suite.ctx)

	suite.client.AssertCalled(suite.T(), "GetAll", suite.ctx)
}

func (suite *ListReadProfilesSuite) TestGetAllProfiles_Execute_Returns_An_Error_If_The_Profiles_Cannot_Be_Retrieved() {
	expectedErr := errors.New("Failed")
	suite.client.On("GetAll", mock.Anything).Return(nil, expectedErr)

	actualErr := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), expectedErr, actualErr)
}

func aListOfProfiles(names ...string) client.ReadProfileList {
	expected := make(client.ReadProfileList, len(names))

	for index, name := range names {
		expected[index] = client.ReadProfile{
			Name:      name,
			Languages: []string{generators.String("language"), generators.String("language")},
		}
	}

	return expected
}
