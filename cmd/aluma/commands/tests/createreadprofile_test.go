package tests

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	mocks2 "gitlab.com/aluma-clients/aluma-cli/client/mocks"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type CreateReadProfileSuite struct {
	suite.Suite
	sut       *commands.CreateReadProfileCmd
	creator   *mocks2.ReadProfileCreator
	ctx       context.Context
	name      string
	languages []string
}

func (suite *CreateReadProfileSuite) SetupTest() {
	suite.creator = new(mocks2.ReadProfileCreator)
	suite.name = generators.String("name")
	suite.languages = []string{generators.String("language"), generators.String("language")}

	suite.sut = &commands.CreateReadProfileCmd{
		Creator:   suite.creator,
		Name:      suite.name,
		Languages: suite.languages,
	}
	suite.ctx = context.Background()
}

func TestCreateReadProfileSuiteRunner(t *testing.T) {
	suite.Run(t, new(CreateReadProfileSuite))
}

func (suite *CreateReadProfileSuite) TestCreateProfile_Execute_Calls_The_Creator() {
	aProfile := client.ReadProfile{
		Name:      generators.String("name"),
		Languages: []string{generators.String("language")},
	}

	suite.creator.On("Create", mock.Anything, mock.Anything, mock.Anything).Return(aProfile, nil)

	suite.sut.Execute(suite.ctx)

	suite.creator.AssertCalled(suite.T(), "Create", suite.ctx, suite.name, suite.languages)
}

func (suite *CreateReadProfileSuite) TestCreateProfile_Execute_Returns_Nil_Error_On_Success() {
	aProfile := client.ReadProfile{
		Name:      generators.String("name"),
		Languages: []string{generators.String("language")},
	}

	suite.creator.On("Create", mock.Anything, mock.Anything, mock.Anything).Return(aProfile, nil)

	actualErr := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), nil, actualErr)
}

func (suite *CreateReadProfileSuite) TestCreateProfile_Execute_Returns_An_Error_If_The_Profiles_Cannot_Be_Created() {
	expectedErr := errors.New("Failed")
	suite.creator.On("Create", mock.Anything, mock.Anything, mock.Anything).Return(client.ReadProfile{}, expectedErr)

	actualErr := suite.sut.Execute(context.Background())

	assert.Equal(suite.T(), expectedErr, actualErr)
}
