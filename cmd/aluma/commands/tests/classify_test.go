package tests

import (
	"context"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands/mocks"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type classifyCommandSuite struct {
	suite.Suite
	classificationService *mocks.ClassificationService
	sut                   *commands.ClassifyCmd
	filePatterns          []string
	ctx                   context.Context
	expectedErr           error
	classifierName        string
	readProfileArgs       client.ReadProfileArgs
}

func (suite *classifyCommandSuite) SetupTest() {
	suite.classificationService = new(mocks.ClassificationService)
	suite.ctx, _ = context.WithCancel(context.Background())
	suite.filePatterns = []string{generators.String("file1"), generators.String("file2")}
	suite.classifierName = generators.String("classifierName")
	suite.readProfileArgs = client.ReadProfileArgs{
		UseReadProfile: generators.Bool(),
		ProfileName:    generators.String("profile"),
	}

	suite.expectedErr = errors.New("simulated error")

	suite.classificationService.
		On("ClassifyAll", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(suite.expectedErr)

	suite.sut = &commands.ClassifyCmd{
		FilePaths:             suite.filePatterns,
		ClassificationService: suite.classificationService,
		ClassifierName:        suite.classifierName,
		ReadProfileArgs:       suite.readProfileArgs,
	}
}

func TestClassifyCommandSuiteRunner(t *testing.T) {
	suite.Run(t, new(classifyCommandSuite))
}

func (suite *classifyCommandSuite) Test_ClassificationService_ClassifyAll_Called_With_Correct_Params() {
	_ = suite.sut.Execute(suite.ctx)

	suite.classificationService.AssertCalled(suite.T(),
		"ClassifyAll",
		suite.ctx,
		suite.filePatterns,
		suite.classifierName,
		suite.readProfileArgs)
}

func (suite *classifyCommandSuite) Test_Error_Returned_From_ClassificationService() {
	actualErr := suite.sut.Execute(suite.ctx)
	assert.EqualError(suite.T(), errors.Cause(actualErr), suite.expectedErr.Error())
}
