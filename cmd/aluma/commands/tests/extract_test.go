package tests

import (
	"context"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/commands/mocks"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type extractCommandSuite struct {
	suite.Suite
	extractionService *mocks.ExtractionService
	sut               *commands.ExtractCmd
	filePatterns      []string
	ctx               context.Context
	expectedErr       error
	extractorName     string
	readProfileArgs   client.ReadProfileArgs
}

func (suite *extractCommandSuite) SetupTest() {
	suite.extractionService = new(mocks.ExtractionService)
	suite.ctx, _ = context.WithCancel(context.Background())
	suite.filePatterns = []string{generators.String("file1"), generators.String("file2")}
	suite.extractorName = generators.String("extractorName")
	suite.readProfileArgs = client.ReadProfileArgs{
		UseReadProfile: generators.Bool(),
		ProfileName:    generators.String("profile"),
	}
	suite.expectedErr = errors.New("simulated error")

	suite.extractionService.
		On("ExtractAll", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(suite.expectedErr)

	suite.sut = &commands.ExtractCmd{
		FilePaths:         suite.filePatterns,
		ExtractionService: suite.extractionService,
		ExtractorName:     suite.extractorName,
		ReadProfileArgs:   suite.readProfileArgs,
	}
}

func TestExtractCommandSuiteRunner(t *testing.T) {
	suite.Run(t, new(extractCommandSuite))
}

func (suite *extractCommandSuite) Test_ExtractionService_ExtractAll_Called_With_Correct_Params() {
	_ = suite.sut.Execute(suite.ctx)

	suite.extractionService.AssertCalled(suite.T(),
		"ExtractAll",
		suite.ctx,
		suite.filePatterns,
		suite.extractorName,
		suite.readProfileArgs)
}

func (suite *extractCommandSuite) Test_Error_Returned_From_ExtractionService() {
	actualErr := suite.sut.Execute(suite.ctx)

	assert.EqualError(suite.T(), errors.Cause(actualErr), suite.expectedErr.Error())
}
