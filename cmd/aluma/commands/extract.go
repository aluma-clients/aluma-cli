package commands

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/cmd/aluma/services"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gitlab.com/aluma-clients/aluma-cli/output/progress"
	"gitlab.com/aluma-clients/aluma-cli/output/resultsWriters"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

type extractArgs struct {
	extractorName string
	outputFormat  string
	filePatterns  []string
	readProfile   string
}

type ExtractCmd struct {
	ExtractorName     string
	FilePaths         []string
	ReadProfileArgs   client.ReadProfileArgs
	ExtractionService ExtractionService
}

//go:generate mockery -name "ExtractionService"
type ExtractionService interface {
	ExtractAll(ctx context.Context, files []string,
		extractorName string, args client.ReadProfileArgs) error
}

func ConfigureExtractCommand(ctx context.Context,
	app *kingpin.Application,
	globalFlags *config.GlobalFlags) {
	args := &extractArgs{}
	cmd := &ExtractCmd{}
	extractCli := app.
		Command("extract", "Perform data extraction on a file or set of files.").
		Action(func(parseContext *kingpin.ParseContext) error {
			err := cmd.initWithArgs(args, globalFlags)
			if err != nil {
				return err
			}
			return cmd.Execute(ctx)
		})

	extractCli.Flag("format", "The output format. Allowed values: table, csv, json [default: table].").
		Short('f').
		Default("table").
		EnumVar(&args.outputFormat, "table", "csv", "json")

	extractCli.Arg("extractor-name", "The name of the extractor to use.").
		Required().
		StringVar(&args.extractorName)

	extractCli.Arg("files", "The files to read.").
		Required().
		StringsVar(&args.filePatterns)

	extractCli.Flag("read-profile", "The read profile to use [default: none (english language)].").
		Short('r').
		Default("").
		StringVar(&args.readProfile)

	addFileHandlingFlagsTo(globalFlags, extractCli)
}

func (cmd *ExtractCmd) initWithArgs(args *extractArgs, flags *config.GlobalFlags) error {
	resultsWriter, err := resultsWriters.NewExtractionResultsWriter(flags.MultiFileOut,
		flags.OutputFile,
		args.outputFormat)

	if err != nil {
		return err
	}

	progressHandler := progress.NewProgressHandler(resultsWriter,
		flags.ShowProgress, os.Stderr)

	cmd.FilePaths, err = GlobMany(args.filePatterns)

	if err != nil {
		return err
	}

	readProfile := args.readProfile
	if readProfile != "" {
		cmd.ReadProfileArgs = client.ReadProfileArgs{
			UseReadProfile: true,
			ProfileName:    readProfile,
		}
	} else {
		cmd.ReadProfileArgs = client.ReadProfileArgs{
			UseReadProfile: false,
		}
	}

	apiClient, err := initApiClient(flags.ClientId,
		flags.ClientSecret,
		flags.LogHttp)

	if err != nil {
		return err
	}

	fileExtractor := client.NewFileExtractor(apiClient.Documents, apiClient.Documents,
		apiClient.Documents)

	cmd.ExtractionService = services.NewParallelExtractionService(fileExtractor, apiClient.Documents,
		progressHandler)
	cmd.ExtractorName = args.extractorName

	return nil
}

// ExecuteExtract is the main entry point for the 'extract' command.
func (cmd *ExtractCmd) Execute(ctx context.Context) error {
	err := cmd.ExtractionService.ExtractAll(ctx, cmd.FilePaths, cmd.ExtractorName, cmd.ReadProfileArgs)

	return errors.Wrap(err, "extraction failed")
}
