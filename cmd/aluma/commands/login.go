package commands

import (
	"context"
	"fmt"
	"gitlab.com/aluma-clients/aluma-cli/auth"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gopkg.in/alecthomas/kingpin.v2"
)

// ConfigureLoginCommand configures kingpin to add the login command.
func ConfigureLoginCommand(ctx context.Context,
	app *kingpin.Application,
	globalFlags *config.GlobalFlags) {

	loginCmd := &LoginCmd{}
	app.Command("login", "Connect aluma to your account.").
		Action(func(parseContext *kingpin.ParseContext) error {
			// execute the command
			return ExecuteWithMessage("Logging in... ", func() error {
				err := loginCmd.initFromArgs(globalFlags)
				if err != nil {
					return err
				}
				return loginCmd.Execute(ctx, globalFlags)
			})
		})
}

type LoginCmd struct {
	TokenRetriever      auth.TokenRetriever
	ConfigurationWriter config.ConfigurationWriter
}

func (cmd *LoginCmd) initFromArgs(flags *config.GlobalFlags) error {

	var err error
	cmd.TokenRetriever = client.NewTokenRetriever(DefaultHttpClient, client.ApiAddress)
	cmd.ConfigurationWriter, err = config.NewAppDirectory()

	return err
}

func (cmd *LoginCmd) Execute(ctx context.Context, flags *config.GlobalFlags) error {
	var (
		err           error
		clientId      = flags.ClientId
		clientSecret  = flags.ClientSecret
		consoleReader = ConsoleSecretReader{}
	)

	if clientId == "" {
		fmt.Print("\nClient Id: ")
		clientId, err = consoleReader.ReadLine(ctx)

		if err != nil {
			return err
		}
	}

	if clientSecret == "" {
		fmt.Print("Client Secret (not shown): ")
		clientSecret, err = consoleReader.ReadSecret(ctx)
		if err != nil {
			return err
		}
		fmt.Print("\n")
	}

	_, err = cmd.TokenRetriever.RetrieveToken(clientId, clientSecret)

	if err != nil {
		return err
	}

	configuration := config.NewConfiguration(clientId, clientSecret)

	err = cmd.ConfigurationWriter.WriteConfiguration(configuration)

	return err
}
