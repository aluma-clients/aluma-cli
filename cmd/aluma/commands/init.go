package commands

import (
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"io"
	"net/http"
	"os"
	"time"
)

func initApiClient(clientIdFlag, clientSecretFlag string, logHttpFile *os.File) (*client.ApiClient, error) {
	appDir, err := config.NewAppDirectory()
	if err != nil {
		return nil, err
	}

	credentialsResolver := &CredentialsResolver{}

	clientId, clientSecret, err := credentialsResolver.Resolve(clientIdFlag, clientSecretFlag, appDir)

	if err != nil {
		return nil, err
	}

	var logSink io.Writer = nil
	if logHttpFile != nil {
		logSink = logHttpFile
	}
	return client.NewApiClient(DefaultHttpClient, client.ApiAddress, clientId, clientSecret, logSink), nil
}

var DefaultHttpClient = &http.Client{Timeout: time.Minute * 2}
