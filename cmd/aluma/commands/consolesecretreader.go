package commands

import (
	"bufio"
	"context"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"strings"
)

type ConsoleSecretReader struct {
}

func (r ConsoleSecretReader) ReadLine(ctx context.Context) (string, error) {
	var (
		input string
		err   error
	)

	err = r.runWithCtx(ctx, func() error {
		scanner := bufio.NewScanner(os.Stdin)

		scanner.Scan()

		input = strings.TrimSpace(scanner.Text())
		return scanner.Err()
	})

	return input, err
}

func (r ConsoleSecretReader) ReadSecret(ctx context.Context) (string, error) {
	var (
		input string
		err   error
	)

	err = r.runWithCtx(ctx, func() error {
		secretBytes, err := terminal.ReadPassword(int(os.Stdin.Fd()))
		input = string(secretBytes)
		return err
	})

	return input, err
}

func (r ConsoleSecretReader) runWithCtx(ctx context.Context, runner func() error) error {
	var resultErr error
	doneCh := make(chan struct{})
	go func() {
		defer close(doneCh)
		resultErr = runner()
	}()

	// Wait on either the runner or the context to finish
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-doneCh:
		return resultErr
	}
}
