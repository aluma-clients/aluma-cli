package commands

import (
	"context"
	"fmt"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/config"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"strings"
)

type ListReadProfilesCmd struct {
	Client client.ReadProfileGetter
}

// Configures kingpin with the 'list read-profiles' command
func ConfigureListReadProfilesCmd(ctx context.Context, parentCmd *kingpin.CmdClause, flags *config.GlobalFlags) {
	listReadProfilesCmd := &ListReadProfilesCmd{}
	parentCmd.Command("read-profiles", "List all available read profiles.").
		Alias("read-profile").
		Action(func(parseContext *kingpin.ParseContext) error {
			err := listReadProfilesCmd.initFromArgs(flags)
			if err != nil {
				return err
			}
			return listReadProfilesCmd.Execute(ctx)
		})
}

// Executes the command.
func (cmd *ListReadProfilesCmd) Execute(ctx context.Context) error {
	readProfiles, err := cmd.Client.GetAll(ctx)
	if err != nil {
		return err
	}

	if len(readProfiles) == 0 {
		fmt.Println("No read-profiles found.")
		return nil
	}

	table := NewTable(os.Stdout, []string{"Name", "Languages"})
	for _, readProfile := range readProfiles {
		table.Append([]string{readProfile.Name, strings.Join(readProfile.Languages, ", ")})
	}
	table.Render()

	return nil
}

func (cmd *ListReadProfilesCmd) initFromArgs(flags *config.GlobalFlags) error {
	apiClient, err := initApiClient(flags.ClientId, flags.ClientSecret, flags.LogHttp)

	if err != nil {
		return err
	}

	cmd.Client = apiClient.ReadProfiles
	return nil
}
