package services

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/pool"
	"io"
	"os"
)

//go:generate mockery -name "FileRedactor"

type FileRedactor interface {
	Redact(ctx context.Context, fileContent io.Reader, extractorName string, args client.ReadProfileArgs) (io.ReadCloser, error)
}

// ParallelRedactionService wraps the ch360.FileRedactor to process multiple files in parallel.
type ParallelRedactionService struct {
	singleFileRedactor     FileRedactor
	documentGetter         client.DocumentGetter
	parallelFilesProcessor ParallelFilesProcessor
}

// NewParallelRedactionService constructs a new ParallelRedactionService.
func NewParallelRedactionService(fileRedactor FileRedactor,
	documentGetter client.DocumentGetter,
	progressHandler ProgressHandler) *ParallelRedactionService {

	return &ParallelRedactionService{
		singleFileRedactor: fileRedactor,
		documentGetter:     documentGetter,
		parallelFilesProcessor: ParallelFilesProcessor{
			ProgressHandler: progressHandler,
		},
	}
}

func (p *ParallelRedactionService) RedactAllWithExtractor(ctx context.Context, files []string,
	extractorName string, args client.ReadProfileArgs) error {

	// Limit the number of workers to the number of available doc slots
	parallelWorkers, err := client.GetFreeDocSlots(ctx, p.documentGetter, client.TotalDocumentSlots)

	if err != nil {
		return err
	}

	// called in parallel, once per file
	processorFunc := func(ctx context.Context, filename string) pool.ProcessorFunc {
		return func() (interface{}, error) {
			file, err := os.Open(filename)
			if err != nil {
				return nil, errors.Wrapf(err, "Error redacting file %s", filename)
			}
			defer file.Close()

			readCloser, err := p.singleFileRedactor.Redact(ctx, file, extractorName, args)

			return readCloser, errors.Wrapf(err, "Error redacting file %s", filename)
		}
	}

	return p.parallelFilesProcessor.Run(ctx, files, parallelWorkers,
		processorFunc)
}
