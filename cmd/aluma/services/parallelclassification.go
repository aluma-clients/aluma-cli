package services

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/client/results"
	"gitlab.com/aluma-clients/aluma-cli/pool"
	"io"
	"os"
)

//go:generate mockery -name "FileClassifier"

type FileClassifier interface {
	Classify(ctx context.Context, fileContent io.Reader, classifierName string, args client.ReadProfileArgs) (*results.ClassificationResult, error)
}

// ParallelClassificationService wraps the ch360.FileClassifier to process multiple files in parallel.
type ParallelClassificationService struct {
	singleFileClassifier   FileClassifier
	documentGetter         client.DocumentGetter
	parallelFilesProcessor ParallelFilesProcessor
}

// NewParallelClassificationService constructs a new ParallelClassificationService.
func NewParallelClassificationService(fileClassifier FileClassifier,
	documentGetter client.DocumentGetter,
	progressHandler ProgressHandler) *ParallelClassificationService {

	return &ParallelClassificationService{
		singleFileClassifier: fileClassifier,
		documentGetter:       documentGetter,
		parallelFilesProcessor: ParallelFilesProcessor{
			ProgressHandler: progressHandler,
		},
	}
}

func (p *ParallelClassificationService) ClassifyAll(ctx context.Context, files []string,
	classifierName string, args client.ReadProfileArgs) error {

	// Limit the number of workers to the number of available doc slots
	parallelWorkers, err := client.GetFreeDocSlots(ctx, p.documentGetter, client.TotalDocumentSlots)

	if err != nil {
		return err
	}

	// called in parallel, once per file
	processorFunc := func(ctx context.Context, filename string) pool.ProcessorFunc {
		return func() (interface{}, error) {
			file, err := os.Open(filename)
			if err != nil {
				return nil, errors.Wrapf(err, "Error classifying file %s", filename)
			}
			defer file.Close()

			readCloser, err := p.singleFileClassifier.Classify(ctx, file, classifierName, args)

			return readCloser, errors.Wrapf(err, "Error classifying file %s", filename)
		}
	}

	return p.parallelFilesProcessor.Run(ctx, files, parallelWorkers,
		processorFunc)
}
