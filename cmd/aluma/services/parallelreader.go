package services

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/pool"
	"io"
	"os"
)

//go:generate mockery -name "FileReader|FileGlobProcessor"

type FileReader interface {
	Read(ctx context.Context, fileContent io.Reader, mode client.ReadMode, args client.ReadProfileArgs) (io.ReadCloser, error)
}

type FileGlobProcessor interface {
	RunWithGlob(ctx context.Context,
		filePatterns []string,
		parallelism int,
		processorFuncFactory ProcessorFuncFactory) error
}

// ParallelReaderService wraps the ch360.FileReader to process multiple files in parallel.
type ParallelReaderService struct {
	singleFileReader       FileReader
	documentGetter         client.DocumentGetter
	parallelFilesProcessor ParallelFilesProcessor
}

// NewParallelReaderService constructs a new ParallelReaderService.
func NewParallelReaderService(fileReader FileReader,
	documentGetter client.DocumentGetter,
	progressHandler ProgressHandler) *ParallelReaderService {

	return &ParallelReaderService{
		singleFileReader: fileReader,
		documentGetter:   documentGetter,
		parallelFilesProcessor: ParallelFilesProcessor{
			ProgressHandler: progressHandler,
		},
	}
}

func (p *ParallelReaderService) ReadAll(ctx context.Context, files []string,
	readMode client.ReadMode, args client.ReadProfileArgs) error {

	// Limit the number of workers to the number of available doc slots
	parallelWorkers, err := client.GetFreeDocSlots(ctx, p.documentGetter, client.TotalDocumentSlots)

	if err != nil {
		return err
	}

	// called in parallel, once per file
	processorFunc := func(ctx context.Context, filename string) pool.ProcessorFunc {
		return func() (interface{}, error) {
			file, err := os.Open(filename)
			if err != nil {
				return nil, errors.Wrapf(err, "Error reading file %s", filename)
			}
			defer file.Close()

			readCloser, err := p.singleFileReader.Read(ctx, file, readMode, args)

			return readCloser, errors.Wrapf(err, "Error reading file %s", filename)
		}
	}

	return p.parallelFilesProcessor.Run(ctx, files, parallelWorkers,
		processorFunc)
}
