// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	context "context"
	"gitlab.com/aluma-clients/aluma-cli/client"
)
import io "io"
import mock "github.com/stretchr/testify/mock"
import results "gitlab.com/aluma-clients/aluma-cli/client/results"

// FileClassifier is an autogenerated mock type for the FileClassifier type
type FileClassifier struct {
	mock.Mock
}

// Classify provides a mock function with given fields: ctx, fileContent, classifierName
func (_m *FileClassifier) Classify(ctx context.Context, fileContent io.Reader, classifierName string, args client.ReadProfileArgs) (*results.ClassificationResult, error) {
	ret := _m.Called(ctx, fileContent, classifierName, args)

	var r0 *results.ClassificationResult
	if rf, ok := ret.Get(0).(func(context.Context, io.Reader, string, client.ReadProfileArgs) *results.ClassificationResult); ok {
		r0 = rf(ctx, fileContent, classifierName, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*results.ClassificationResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, io.Reader, string, client.ReadProfileArgs) error); ok {
		r1 = rf(ctx, fileContent, classifierName, args)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
