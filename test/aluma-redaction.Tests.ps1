Describe "redaction" {
    BeforeAll {
        . $PSScriptRoot/common.ps1

        Login-Aluma
        $extractorName = "test-extractor"
        $documentsPath = (Join-Path $PSScriptRoot (Join-Path "documents" "redaction"))
    }

    BeforeEach {
        Remove-UserExtractors
    }

    It "should redact a file using an extractor" {
        $docFile = (Join-Path $documentsPath "document1.pdf")
        New-ExtractorFromModules $extractorName aluma.name | Format-MultilineOutput | Should -Be @"
Creating extractor '$extractorName'... [OK]
"@
        Invoke-App redact with-extractor $extractorName $docFile -o redacted.pdf

        # Verify
        $LASTEXITCODE | Should -Be 0
        Test-PDFFile $docFile | Should -Be $true
        Remove-Item -Path redacted.pdf
    }

    AfterAll {
        Remove-UserExtractors
        Restore-ApplicationFolder
    }
}
