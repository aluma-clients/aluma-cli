Describe "aluma --version" {
  BeforeAll {
      . $PSScriptRoot/common.ps1
  }

  It "should output date of release" {
    Invoke-App --version | Should -Match "\d{4}\.\d{2}\.\d{2}"
  }

  It "should output short git hash" {
    Invoke-App --version | Should -Match ".*-([a-f0-9]{7,})"
  }

  It "should output build number" {
    Invoke-App --version | Should -Match ".*-(\d+)"
  }

  It "should output the supported format" {
    Invoke-App --version | Should -Match "\d{4}\.\d{2}\.\d{2}-([a-f0-9]{7,})-(\d+)"
  }
}
