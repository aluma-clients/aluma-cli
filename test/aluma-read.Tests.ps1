Describe "reading" {
    BeforeAll {
        . $PSScriptRoot/common.ps1
        . $PSScriptRoot/helpers/reads.ps1

        Login-Aluma
        $readProfileName = "german-read-profile"
        $documentsPath = (Join-Path $PSScriptRoot (Join-Path "documents" "reading"))
    }

    BeforeEach {
        # Tidy up any leftover read-profile from previous runs
        Remove-GermanReadProfile $readProfileName
    }

    It "should read with english when no profile is specified" {        
        $file = (Join-Path $documentsPath "german.pdf")

        # "Schaumtechnik" is the incorrect version of "Schäumtechnik" which you get if you
        # read this document with english not german
        Invoke-Read $file | Format-MultilineOutput | Should -Match ".*Schaumtechnik.*"
        $LASTEXITCODE | Should -Be 0
    }

    It "should read with german when a german read profile is created and used" {
        New-GermanReadProfile
        $LASTEXITCODE | Should -Be 0

        $file = (Join-Path $documentsPath "german.pdf")
        # Check that "Regelmäßige" is read correctly (with the äß) because this is not
        # read as "Regelmafiige" if english is used
        Invoke-GermanRead $file | Format-MultilineOutput | Should -Match ".*Schäumtechnik.*"
        $LASTEXITCODE | Should -Be 0
    }

    AfterAll {
        # Tidy up any leftover read-profiles in the account
        Remove-GermanReadProfile $readProfileName

        # Restore user's original application folder
        Restore-ApplicationFolder
    }
}