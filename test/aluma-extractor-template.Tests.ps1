Describe "extractor-templates" {
    BeforeAll {
        . $PSScriptRoot/common.ps1

        Login-Aluma
        $extractorTemplatePath = "test.json"
    }

    BeforeEach {
    }

    It "should create an extractor template from module ids" {
        Invoke-App create extractor-template aluma.name 2>&1 | Format-MultilineOutput | Should -Be @"
{
  "modules": [
    {
      "id": "aluma.name"
    }
  ]
}
"@
        $LASTEXITCODE | Should -Be 0
    }

    It "write the template to a file if specified" {
        Invoke-App create extractor-template aluma.name -o $extractorTemplatePath

        Get-Content -Raw $extractorTemplatePath | Should -Be @"
{
  "modules": [
    {
      "id": "aluma.name"
    }
  ]
}
"@
    }

    AfterEach {
        if (Test-Path $extractorTemplatePath) {
            Remove-Item $extractorTemplatePath
        }
    }

    AfterAll {
        Restore-ApplicationFolder
    }
}
