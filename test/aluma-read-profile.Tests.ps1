Describe "read-profiles" {    
    BeforeAll {
        . $PSScriptRoot/common.ps1
        . $PSScriptRoot/helpers/reads.ps1
        Login-Aluma

        $readProfileName = "test-read-profile"
    }

    BeforeEach {
        # Tidy up any leftover read-profiles in the account
        Remove-ReadProfile $readProfileName
    }

    It "should be created with a single language" {
        New-ReadProfileFromLanguage $readProfileName "eng" | Format-MultilineOutput | Should -Be @"
Creating read profile '$readProfileName'... [OK]
"@
        $LASTEXITCODE | Should -Be 0

        # Verify
        Get-ReadProfiles | Format-MultilineOutput | Should -Match @"
  Name               Languages\s*
---------------------------------
  test-read-profile  eng\s*
"@
    }

    It "should be created with multiple language" {
        New-ReadProfileFromLanguages  -readProfileName $readProfileName -language1 "eng" -language2 "spa" | Format-MultilineOutput | Should -Be @"
Creating read profile '$readProfileName'... [OK]
"@
        $LASTEXITCODE | Should -Be 0

        # Verify
        Get-ReadProfiles | Format-MultilineOutput | Should -Match @"
  Name               Languages\s*
---------------------------------
  test-read-profile  eng, spa\s*
"@
    }

    It "should print a useful message if no read-profiles present" {
        Get-ReadProfiles | Format-MultilineOutput | Should -Match @"
No read-profiles found.
"@
    }

    It "should delete a read-profile" {
        New-ReadProfileFromLanguage $readProfileName "eng" | Format-MultilineOutput | Should -Be @"
Creating read profile '$readProfileName'... [OK]
"@
        $LASTEXITCODE | Should -Be 0

        Remove-ReadProfile $readProfileName
        $LASTEXITCODE | Should -Be 0

        Get-ReadProfiles | Format-MultilineOutput | Should -Match @"
No read-profiles found.
"@
    }

    AfterAll {
        # Tidy up any leftover read-profiles in the account
        Remove-ReadProfile $readProfileName

        # Restore user's original application folder
        Restore-ApplicationFolder
    }
}