function New-Extractor([string]$extractorName, [Io.FileInfo]$extractorDefinition) {
    Invoke-App upload extractor $extractorName $extractorDefinition 2>&1
}

function New-ExtractorFromTemplate([string]$extractorName, [Io.FileInfo]$extractorTemplate) {
    Invoke-App create extractor from-template $extractorName $extractorTemplate 2>&1
}

function New-GermanReadProfile() {
    Invoke-App create read-profile $readProfileName --language "deu" 2>&1
}

function Remove-GermanReadProfile() {
    Invoke-App delete read-profile $readProfileName 2>&1
}

function Invoke-Extractor(
        [Parameter(ParameterSetName="SingleFile", Position=0)]
        [Io.FileInfo]$file,

        [Parameter(Mandatory=$true, Position=1)]
        [string]$extractorName,

        [Parameter(ParameterSetName="MultipleFiles", Position=0)]
        [string]$filePattern,

        [Parameter(ParameterSetName="MultipleFiles", Position=2)]
        [Io.FileInfo]$outputFile,

        [Parameter(ParameterSetName="MultipleFiles", Position=3)]
        [string]$Format = "csv"
    )
    {
        if ($file -ne $null) {
            Invoke-App extract $extractorName $($file.FullName)
        } else {
            if ($outputFile -ne $null) {
                Invoke-App extract $extractorName "`"$filePattern`"" -o $outputFile -f $Format
            } else {
                Invoke-App extract $extractorName "`"$filePattern`"" -m -f $Format
            }
        }
    }

function ConvertFrom-ExtractionCsv([Parameter(ValueFromPipeline=$true)][PsObject[]]$InputObject) {
    PROCESS {
        $InputObject | ConvertFrom-Csv -Header "file", "amount"
    }
}