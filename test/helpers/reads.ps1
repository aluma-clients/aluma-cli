function New-GermanReadProfile() {
    Invoke-App create read-profile $readProfileName --language "deu" 2>&1
}

function Remove-GermanReadProfile() {
    Invoke-App delete read-profile $readProfileName 2>&1
}

function Invoke-Read([Parameter(ParameterSetName="SingleFile", Position=0)]
        [Io.FileInfo]$file) {
    Invoke-App read $($file.FullName)
}

function Invoke-GermanRead([Parameter(ParameterSetName="SingleFile", Position=0)]
        [Io.FileInfo]$file) {
    Invoke-App read $($file.FullName) -r $readProfileName
}

function New-ReadProfileFromLanguage([string]$readProfileName, [string]$language) {
    Invoke-App create read-profile $readProfileName --language $language 2>&1
}

function New-ReadProfileFromLanguages([string]$readProfileName, [string]$language1, [string]$language2) {
    Invoke-App create read-profile $readProfileName --language $language1 --language $language2 2>&1
}

function Get-ReadProfiles {
    Invoke-App list read-profiles 2>&1
}

function Remove-ReadProfile([Parameter(ValueFromPipeline=$true)]$readProfileName) {
    Invoke-App delete read-profile $readProfileName 2>&1
}

