function New-Document([Io.FileInfo]$document) {
    Invoke-App create document $document 2>&1
}

function Get-Documents {
    Invoke-App list documents 2>&1
}

function Remove-Document([Parameter(ValueFromPipeline=$true)]$documentId) {
    Invoke-App delete document $documentId 2>&1
}