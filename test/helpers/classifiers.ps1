function New-Classifier([string]$classifierName, [Io.FileInfo]$samples) {
    Invoke-App create classifier $classifierName $samples 2>&1
}

function Get-Classifiers {
    Invoke-App list classifiers 2>&1
}

function Remove-Classifier([Parameter(ValueFromPipeline=$true)]$classifierName) {
    Invoke-App delete classifier $classifierName 2>&1
}

function Invoke-Classifier(
    [Parameter(ParameterSetName="SingleFile", Position=0)]
    [Io.FileInfo]$file,

    [Parameter(Mandatory=$true, Position=1)]
    [string]$classifierName,

    [Parameter(ParameterSetName="MultipleFiles", Position=0)]
    [string]$filePattern,

    [Parameter(ParameterSetName="MultipleFiles", Position=2)]
    [Io.FileInfo]$outputFile,

    [Parameter(ParameterSetName="MultipleFiles", Position=3)]
    [string]$Format = "csv")
{
    if ($file -ne $null) {
        Invoke-App classify $classifierName $($file.FullName)
    } else {
        if ($outputFile -ne $null) {
            Invoke-App classify $classifierName "`"$filePattern`"" -o $outputFile -f $Format
        } else {
            Invoke-App classify $classifierName "`"$filePattern`"" -m -f $Format
        }
    }
}

function ConvertFrom-ClassificationCsv([Parameter(ValueFromPipeline=$true)][PsObject[]]$InputObject) {
    PROCESS {
        $InputObject | ConvertFrom-Csv -Header "file","documenttype","confident","score"
    }
}

