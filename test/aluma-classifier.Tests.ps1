Describe "classifiers" {
    BeforeAll {
        . $PSScriptRoot/helpers/classifiers.ps1
        . $PSScriptRoot/common.ps1

        Login-Aluma

        $classifierName = "test-classifier"
        $documentsPath = (Join-Path $PSScriptRoot (Join-Path "documents" "classification"))
    }

    BeforeEach {
        # Tidy up any leftover classifiers in the account
        Get-Classifiers | Remove-Classifier
    }

    It "should be created from a zip file of samples" {
        $samples = (Join-Path $PSScriptRoot "samples.zip")
        New-Classifier $classifierName $samples | Format-MultilineOutput | Should -Be @"
Creating classifier '$classifierName'... [OK]
"@
        $LASTEXITCODE | Should -Be 0

        # Verify
        Get-Classifiers | Format-MultilineOutput | Should -Contain $classifierName
    }

    It "should not be created from an invalid zip file of samples" {
        $samples = (Join-Path $PSScriptRoot "invalid.zip")
        New-Classifier $classifierName $samples | Format-MultilineOutput | Should -Match (String-Starting @"
Creating classifier '$classifierName'... [FAILED]
"@)

        $LASTEXITCODE | Should -Be 1
    }

    It "should not be created from a non-existent zip file of samples" {
        $samples = (Join-Path $PSScriptRoot "non-existent.zip")
        New-Classifier $classifierName $samples | Format-MultilineOutput | Should -Be @"
Creating classifier 'test-classifier'... [FAILED]
Error: failed to open samples archive '$samples': no such file or directory.
"@

        $LASTEXITCODE | Should -Be 1
    }

    It "should attempt to classify a file" {
        $samples = (Join-Path $PSScriptRoot "samples.zip")
        New-Classifier $classifierName $samples

        $document = Join-Path $documentsPath "document1.pdf"
        Invoke-Classifier -File $document $classifierName | Format-MultilineOutput | Should -Be @"
FILE                                 DOCUMENT TYPE                    CONFIDENT
document1.pdf                        Notice of Lien                   true
"@
    }

    It "should classify files and write to a multiple csv results files" {
        $samples = (Join-Path $PSScriptRoot "samples.zip")
        New-Classifier $classifierName $samples

        $filePattern = (Join-Path $documentsPath "subfolder1/*.pdf")
        $document2OutputFile = (Join-Path $documentsPath "subfolder1/document2.csv")
        $document3OutputFile = (Join-Path $documentsPath "subfolder1/document3.csv")

        Invoke-Classifier $filePattern $classifierName -Format "csv"

        Get-Content $document2OutputFile | ConvertFrom-ClassificationCsv `
          | where {($_.file -like "*document2.pdf" -and $_.documenttype -eq "Notice of Lien" -and $_.score -eq 1.177 -and $_.confident)} `
          | Should -Not -Be $null

        Get-Content $document3OutputFile | ConvertFrom-ClassificationCsv `
          | where {($_.file -like "*document3.pdf" -and $_.documenttype -eq "Notice of Default" -and $_.score -eq 3.351 -and $_.confident)} `
          | Should -Not -Be $null

        Remove-Item -Path $document2OutputFile
        Remove-Item -Path $document3OutputFile
    }

    It "should classify files and write to a single csv results file" {
        $samples = (Join-Path $PSScriptRoot "samples.zip")
        New-Classifier $classifierName $samples

        $filePattern = (Join-Path $documentsPath "subfolder1/*.pdf")
        $outputFile = New-TemporaryFile
        Invoke-Classifier $filePattern $classifierName -OutputFile $outputFile -Format "csv"

        $results = Get-Content $outputFile | ConvertFrom-ClassificationCsv

        $results.length | Should -Be 3
        $doc2result = ($results | where {($_.file -like "*document2.pdf" -and $_.documenttype -eq "Notice of Lien" -and $_.score -eq 1.177 -and $_.confident)})
        $doc2result | Should -Not -Be $null

        $doc3result = ($results | where {($_.file -like "*document3.pdf" -and $_.documenttype -eq "Notice of Default" -and $_.score -eq 3.351 -and $_.confident)})
        $doc3result | Should -Not -Be $null

        Remove-Item -Path $outputFile
    }

    It "should classify a file and write a json result file" {
        $samples = (Join-Path $PSScriptRoot "samples.zip")
        New-Classifier $classifierName $samples

        $filePattern = (Join-Path $documentsPath "subfolder1/document2.pdf")
        $outputFile = (Join-Path $documentsPath "subfolder1/document2.json")

        Invoke-Classifier $filePattern $classifierName -OutputFile $outputFile -Format "json"

        $results = Get-Content -Path $outputFile | ConvertFrom-Json

        $results.filename | Should -BeLike "*document2.pdf"
        $results.classification_results.document_type | Should -Be "Notice of Lien"
        $results.classification_results.is_confident | Should -Be True
        $results.classification_results.relative_confidence | Should -Be 1.1768367290496826

        Remove-Item -Path $outputFile
    }

    AfterAll {
        # Tidy up any leftover classifiers in the account
        Get-Classifiers | Remove-Classifier

        # Restore user's original application folder
        Restore-ApplicationFolder
    }
}