# Aluma CLI, the command line client for aluma.io

The aluma CLI allows you to quickly get started using [aluma.io](https://aluma.io). It can:
 * Perform OCR on documents and images, and generate searchable PDFs
 * Extract data from documents
 * Classify documents
 * Create custom extractors from modules
 * ...and more!

This repo also includes the aluma.io client library for go, and is intended as a 'best-practice' integration with the [aluma.io API](https://docs.aluma.io/reference).

More documentation is available [here](https://docs.aluma.io).