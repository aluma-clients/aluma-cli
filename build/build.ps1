param(
  [String]$BuildDate = (Get-Date -Format "yyyy.MM.dd"),
  [String]$GitRev = "$(git rev-parse --short HEAD)",
  [String]$BuildNumber = "$([int]$env:BUILD_NUMBER)",
  [String]$ClientId,
  [String]$ClientSecret
)

$RootDir = Join-Path $PsScriptRoot ".."

Task PackageRestore {
  try {
    pushd $RootDir
    exec { go get -t -d ./... }
  } finally {
    popd
  }
}

Task GenerateMocks {
  try {
    pushd $RootDir
    exec { go get github.com/vektra/mockery/cmd/mockery }
    exec { go get golang.org/x/tools/cmd/stringer }
    exec { go generate ./... }
  } finally {
    popd
  }
}

Task Build PackageRestore, {
  try {
    pushd $RootDir

    $version="${BuildDate}-${GitRev}-${BuildNumber}"

    # eg aluma-windows-amd64.exe
    exec { go build -o $PsScriptRoot/../bin/aluma-$(go env GOOS)-$(go env GOARCH)$(go env GOEXE) -ldflags "-X gitlab.com/aluma-clients/aluma-cli/client.Version=$version" ./cmd/aluma }
  } finally {
    popd
  }
}

Task Test Build, {
  try {
    pushd $RootDir
    $loginCredentialsFile = "login-credentials.json"

    $vet = Allow-Vet
    exec { go test -v $vet -race ./... }

    # Pester 5.0 doesn't support parameterised test scripts, so write credentials into a file that
    # is parsed back in by Login-Aluma in common.ps1
    @{ClientId = $ClientId; ClientSecret = $ClientSecret} | ConvertTo-Json | Out-File $loginCredentialsFile

    $testResults = Invoke-Pester -PassThru -Script "test/*.Tests.ps1" -Output  Detailed
    assert ($testResults.FailedCount -eq 0)
  } finally {
    if (Test-Path $loginCredentialsFile) {
      Remove-Item $loginCredentialsFile
    }
    popd
  }
}

function Allow-Vet {
  $os = $PSVersionTable.OS
  if (($os -eq $null) -or ($os.StartsWith("Microsoft Windows"))) {
    Write-Warning "OS is Windows, disabling go vet (see golang/go#27089)"
    return "-vet=off"
  }

  return ""
}

Task . PackageRestore, Build
