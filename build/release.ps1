param(
    [string]$VersionString,
    # GitLab Personal Access Token
    [string]$GitLabToken,
    [string]$Commit = "master",
    [string]$ReleaseNotes
)

$GITLAB_PROJECT_URI = "https://gitlab.com/api/v4/projects/aluma-clients%2Faluma-cli"
$GO_BIN = (Join-Path $PSScriptRoot ../bin)

function Get-UploadUrl([string]$urlTemplate, [string]$assetName) {
    $queryParamsPosition = $urlTemplate.LastIndexOf("{")
    $url = $urlTemplate.Substring(0, $queryParamsPosition)

    return "$($url)?name=$assetName"
}

function Upload-GCloudFile([string]$sourceFile, [string]$gcloudStorageUrl) {
    Write-Host "Uploading $sourceFile to $gcloudStorageUrl"
    gsutil cp $sourceFile $gcloudStorageUrl
    Write-Host "Uploaded $sourceFile to $gcloudStorageUrl"
}

function Upload-GCloudFiles() {
    $macosBin = Join-Path $GO_BIN aluma-darwin-amd64
    $macosBin = Join-Path $GO_BIN aluma-darwin-amd64
    Upload-GCloudFile $macosBin "gs://downloads.us.aluma.io/cli/macos/aluma"

    $linuxBin = Join-Path $GO_BIN aluma-linux-amd64
    Upload-GCloudFile $linuxBin "gs://downloads.us.aluma.io/cli/linux/aluma"

    $windowsBin = Join-Path $GO_BIN aluma-windows-amd64.exe
    Upload-GCloudFile $windowsBin "gs://downloads.us.aluma.io/cli/windows/aluma.exe"
}

Task Release {    
    # Upload the binaries to the GCloud storage bucket that backs downloads.aluma.io
    # Existing binaries there are overwritten
    Upload-GCloudFiles

    # Create a new release in GitLab
    # Upload the binaries as files on the project, and store the urls for them
    $binaries = Get-ChildItem $GO_BIN -Recurse -Include aluma*

    # Create a tag from the specified commit
    $tag = [PSCustomObject] (Invoke-RestMethod `
    -Method POST `
    -Headers @{ "Private-Token"="$GitLabToken" } `
    -Uri "$GITLAB_PROJECT_URI/repository/tags?tag_name=v$VersionString&ref=$Commit")

    Write-Host (ConvertTo-Json $tag)

    $assetLinks = @()
    $binaries |% {
        Write-Host "Uploading $_ ..."
        $uploadUrl = "$GITLAB_PROJECT_URI/uploads"
        $form = @{
          file = Get-Item -Path $_
        }

        Write-Host "Creating file at $uploadUrl"
        $uploadedFile = Invoke-RestMethod `
            -Method POST `
            -Headers @{ "Private-Token"="$GitLabToken" } `
            -Uri $uploadUrl `
            -Form $form

        $url = $uploadedFile.url
        Write-Host $url

        $assetLink = [PSCustomObject] @{
          "name" = $_.name
          "url" = "https://gitlab.com/aluma-clients/aluma-cli$url"
        }

        $assetLinks += $assetLink

        Write-Host "Uploaded $_ to $uploadedFile.url."
    }

    $assets = [PSCustomObject] @{
      "links" = $assetLinks
    }

    Write-Host (ConvertTo-Json $assets)

    # Create a release from the tag and include the files via the "assets" property
    $releaseMetadata = [PSCustomObject] @{
       "name" = $VersionString
       "tag_name" = "v$VersionString"
       "description" = $ReleaseNotes
       "assets" = $assets
    }

    $createReleaseBody = (ConvertTo-Json $releaseMetadata -Depth 99)
    Write-Host $createReleaseBody

    $release = [PSCustomObject] (Invoke-RestMethod `
        -Method POST `
        -Headers @{ "Private-Token"="$GitLabToken" } `
        -Uri "$GITLAB_PROJECT_URI/releases" `
        -ContentType "application/json" `
        -Body $createReleaseBody)

    Write-Host (ConvertTo-Json $release)
}

Task . Release