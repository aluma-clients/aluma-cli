package client

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
)

//go:generate mockery -name "DocumentReader"

type DocumentReader interface {
	Read(ctx context.Context, documentId string, args ReadProfileArgs) error
	ReadResult(ctx context.Context, documentId string, mode ReadMode, args ReadProfileArgs) (io.ReadCloser, error)
}

type ReadProfileArgs struct {
	// If UseReadProfile set, use profile called ProfileName.
	// Otherwise, use the default profile (i.e. don't specify a profile in HTTP requests)
	UseReadProfile bool
	ProfileName    string
}

type ReadDocumentRequest struct {
	ProfileName string `json:"read_profile"`
}

func (client *DocumentsClient) Read(ctx context.Context, documentId string, args ReadProfileArgs) error {

	var body io.Reader
	var headers map[string]string
	if args.UseReadProfile {
		bodyBytes, err := json.Marshal(&ReadDocumentRequest{
			ProfileName: args.ProfileName,
		})

		if err != nil {
			return err
		}
		body = bytes.NewBuffer(bodyBytes)
		headers = map[string]string{
			"Content-Type": "application/json",
		}
	}

	response, err := newRequest(ctx, "PUT", client.baseUrl+"/documents/"+documentId+"/reads", body).
		withHeaders(headers).
		issue(client.requestSender)

	if err != nil {
		return err
	}

	response.Body.Close()

	return nil
}

type ReadMode int

const (
	ReadPDF ReadMode = iota
	ReadText
	ReadWvdoc
)

func (mode ReadMode) IsBinary() bool {
	return mode != ReadText
}

var readModeHeaders = map[ReadMode]string{
	ReadPDF:   "application/pdf",
	ReadText:  "text/plain",
	ReadWvdoc: "application/vnd.waives.resultformats.read+zip",
}

func (client *DocumentsClient) ReadResult(ctx context.Context,
	documentId string, mode ReadMode, args ReadProfileArgs) (io.ReadCloser, error) {
	headers := map[string]string{
		"Accept": readModeHeaders[mode],
	}

	url := client.baseUrl + "/documents/" + documentId + "/reads"
	if args.UseReadProfile {
		url = url + "?read-profile=" + args.ProfileName
	}

	response, err := newRequest(ctx, "GET", url, nil).
		withHeaders(headers).
		issue(client.requestSender)

	if err != nil {
		return nil, err
	}

	return response.Body, err
}
