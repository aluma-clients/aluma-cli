package client_test

import (
	"bytes"
	"context"
	"github.com/petergtz/pegomock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/client/mocks"
	"gitlab.com/aluma-clients/aluma-cli/client/mocks/matchers"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type CreateDocumentForSuite struct {
	suite.Suite
	documentCreator *mocks.MockDocumentCreator
	documentDeleter *mocks.MockDocumentDeleter
	fileContents    *bytes.Buffer
	document        client.Document
	ctx             context.Context
}

func (suite *CreateDocumentForSuite) SetupTest() {
	documentId := generators.String("documentId")
	suite.document = client.Document{
		Id: documentId,
	}
	suite.fileContents = bytes.NewBuffer(generators.Bytes())

	suite.documentCreator = mocks.NewMockDocumentCreator(pegomock.WithT(suite.T()))
	suite.documentDeleter = mocks.NewMockDocumentDeleter(pegomock.WithT(suite.T()))
	suite.ctx = context.Background()

	pegomock.
		When(suite.documentCreator.Create(matchers.AnyContextContext(), matchers.AnyIoReader())).
		ThenReturn(suite.document, nil)
}

func TestCreateDocumentForSuiteRunner(t *testing.T) {
	suite.Run(t, new(CreateDocumentForSuite))
}

func (suite *CreateDocumentForSuite) Test_CreateDocumentFor_Creates_A_Doc_And_Then_Deletes_It() {

	client.CreateDocumentFor(suite.fileContents, suite.documentCreator, suite.documentDeleter,
		func(document client.Document) error {
			return nil
		})

	suite.documentCreator.
		VerifyWasCalledOnce().
		Create(suite.ctx, suite.fileContents)
	suite.documentDeleter.
		VerifyWasCalledOnce().
		Delete(suite.ctx, suite.document.Id)
}

func (suite *CreateDocumentForSuite) Test_CreateDocumentFor_Returns_Error_From_Function_Param() {
	expectedErr := errors.New("simulated error")

	actualErr := client.CreateDocumentFor(suite.fileContents, suite.documentCreator,
		suite.documentDeleter,
		func(document client.Document) error {
			return expectedErr
		})

	assert.Equal(suite.T(), expectedErr, actualErr)
}

func (suite *CreateDocumentForSuite) Test_CreateDocumentFor_Deletes_The_Document_If_The_Fn_Returns_Err() {
	expectedErr := errors.New("simulated error")

	_ = client.CreateDocumentFor(suite.fileContents, suite.documentCreator,
		suite.documentDeleter,
		func(document client.Document) error {
			return expectedErr
		})

	suite.documentDeleter.
		VerifyWasCalledOnce().
		Delete(suite.ctx, suite.document.Id)
}
