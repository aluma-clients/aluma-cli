package client

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

func TestUtf8ReadCloser_Read(t *testing.T) {
	someBytes := []byte("some bytes")
	utf8BomHeader := []byte{0xEF, 0xBB, 0xBF}
	expectedBytes := append(utf8BomHeader, someBytes...)
	outBuf := bytes.Buffer{}

	sut := newUtf8Wrapper(ioutil.NopCloser(bytes.NewBuffer(someBytes)))
	_, _ = outBuf.ReadFrom(sut)

	assert.Equal(t, expectedBytes, outBuf.Bytes())
}
