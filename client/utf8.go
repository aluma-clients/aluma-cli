package client

import (
	"bytes"
	"io"
)

var _ io.ReadCloser = (*utf8Wrapper)(nil)

// utf8Wrapper wraps an existing io.ReadCloser and prepends the utf-8 BOM.
type utf8Wrapper struct {
	multiReader io.Reader
	wrapped     io.ReadCloser
}

func (u *utf8Wrapper) Read(p []byte) (n int, err error) {
	return u.multiReader.Read(p)
}

func (u *utf8Wrapper) Close() error {
	return u.wrapped.Close()
}

func newUtf8Wrapper(wrapped io.ReadCloser) *utf8Wrapper {
	bomHeader := bytes.NewBuffer([]byte{0xEF, 0xBB, 0xBF})

	return &utf8Wrapper{
		wrapped:     wrapped,
		multiReader: io.MultiReader(bomHeader, wrapped),
	}
}
