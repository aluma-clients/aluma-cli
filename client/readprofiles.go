package client

import (
	"bytes"
	"context"
	"encoding/json"
	"gitlab.com/aluma-clients/aluma-cli/net"
	"strings"
)

type ReadProfileGetter interface {
	GetAll(ctx context.Context) (ReadProfileList, error)
}

type ReadProfileCreator interface {
	Create(ctx context.Context, name string, languages []string) (ReadProfile, error)
}

type ReadProfileDeleter interface {
	Delete(ctx context.Context, name string) error
}

type ReadProfileGetterDeleter interface {
	ReadProfileGetter
	ReadProfileDeleter
}

type ReadProfile struct {
	Name      string
	Languages []string
}
type ReadProfileList []ReadProfile

func readProfileFromReadProfileResponse(response *readProfileResponse) ReadProfile {
	return ReadProfile{
		Name:      response.Name,
		Languages: response.Languages,
	}
}

type getAllReadProfilesResponse struct {
	ReadProfiles []readProfileResponse `json:"read-profiles"`
}

type CreateReadProfileRequest struct {
	Languages []string `json:"languages"`
}

type readProfileResponse struct {
	Name      string   `json:"name"`
	Languages []string `json:"languages"`
}

type ReadProfilesClient struct {
	baseUrl       string
	requestSender net.HttpDoer
}

func NewReadProfilesClient(baseUrl string, httpDoer net.HttpDoer) *ReadProfilesClient {
	return &ReadProfilesClient{
		baseUrl:       baseUrl,
		requestSender: httpDoer,
	}
}

func (client *ReadProfilesClient) Create(ctx context.Context, name string, languages []string) (ReadProfile, error) {
	request := &CreateReadProfileRequest{
		Languages: languages,
	}

	bodyBytes, err := json.Marshal(&request)
	if err != nil {
		return ReadProfile{}, err
	}

	bodyBuffer := bytes.NewBuffer(bodyBytes)
	_, err = newRequest(ctx, "POST", client.baseUrl+"/read-profiles/"+name, bodyBuffer).
		withHeaders(map[string]string{
			"Content-Type": "application/json",
		}).
		issue(client.requestSender)
	if err != nil {
		return ReadProfile{}, err
	}

	return ReadProfile{
		Name:      name,
		Languages: languages,
	}, nil
}

func (client *ReadProfilesClient) GetAll(ctx context.Context) (ReadProfileList, error) {
	response, err := newRequest(ctx, "GET",
		client.baseUrl+"/read-profiles", nil).
		issue(client.requestSender)

	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	buf := bytes.Buffer{}
	_, err = buf.ReadFrom(response.Body)

	if err != nil {
		return nil, err
	}

	var profilesResponse getAllReadProfilesResponse
	err = json.Unmarshal(buf.Bytes(), &profilesResponse)
	if err != nil {
		return nil, err
	}

	var profiles []ReadProfile
	for _, profile := range profilesResponse.ReadProfiles {
		profiles = append(profiles, readProfileFromReadProfileResponse(&profile))
	}

	return profiles, nil
}

func (client *ReadProfilesClient) Delete(ctx context.Context, name string) error {
	_, err := newRequest(ctx, "DELETE",
		client.baseUrl+"/read-profiles/"+name, nil).
		issue(client.requestSender)

	return err
}

func (l *ReadProfileList) Contains(name string) bool {
	for _, p := range *l {
		if strings.ToLower(p.Name) == strings.ToLower(name) {
			return true
		}
	}
	return false
}
