// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	context "context"
	"gitlab.com/aluma-clients/aluma-cli/client"
)
import mock "github.com/stretchr/testify/mock"
import results "gitlab.com/aluma-clients/aluma-cli/client/results"

// DocumentExtractor is an autogenerated mock type for the DocumentExtractor type
type DocumentExtractor struct {
	mock.Mock
}

// Extract provides a mock function with given fields: ctx, documentId, extractorName
func (_m *DocumentExtractor) Extract(ctx context.Context, documentId string, extractorName string, args client.ReadProfileArgs) (*results.ExtractionResult, error) {
	ret := _m.Called(ctx, documentId, extractorName, args)

	var r0 *results.ExtractionResult
	if rf, ok := ret.Get(0).(func(context.Context, string, string, client.ReadProfileArgs) *results.ExtractionResult); ok {
		r0 = rf(ctx, documentId, extractorName, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*results.ExtractionResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, client.ReadProfileArgs) error); ok {
		r1 = rf(ctx, documentId, extractorName, args)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ExtractForRedaction provides a mock function with given fields: ctx, documentId, extractorName
func (_m *DocumentExtractor) ExtractForRedaction(ctx context.Context, documentId string, extractorName string, args client.ReadProfileArgs) (*results.ExtractForRedactionResult, error) {
	ret := _m.Called(ctx, documentId, extractorName, args)

	var r0 *results.ExtractForRedactionResult
	if rf, ok := ret.Get(0).(func(context.Context, string, string, client.ReadProfileArgs) *results.ExtractForRedactionResult); ok {
		r0 = rf(ctx, documentId, extractorName, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*results.ExtractForRedactionResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, client.ReadProfileArgs) error); ok {
		r1 = rf(ctx, documentId, extractorName, args)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
