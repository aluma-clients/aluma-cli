package client_test

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/client/mocks"
	"gitlab.com/aluma-clients/aluma-cli/client/results"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"testing"
)

type FileClassifierSuite struct {
	suite.Suite
	sut                  *client.FileClassifier
	documentCreator      *mocks.DocumentCreator
	documentDeleter      *mocks.DocumentDeleter
	documentClassifier   *mocks.DocumentClassifier
	documentGetter       *mocks.DocumentGetter
	classifierName       string
	documentId           string
	document             client.Document
	classificationResult *results.ClassificationResult
	testFileContent      []byte
	testFileContentBuf   *bytes.Buffer
	ctx                  context.Context
	readProfileArgs      client.ReadProfileArgs
}

func (suite *FileClassifierSuite) SetupTest() {
	suite.classifierName = generators.String("classifier-name")
	suite.documentId = generators.String("documentId")
	suite.document = client.Document{
		Id: suite.documentId,
	}
	suite.classificationResult = &results.ClassificationResult{}
	suite.testFileContent = generators.Bytes()
	suite.testFileContentBuf = bytes.NewBuffer(suite.testFileContent)
	suite.readProfileArgs = client.ReadProfileArgs{
		UseReadProfile: generators.Bool(),
		ProfileName:    generators.String("profile"),
	}

	suite.documentCreator = new(mocks.DocumentCreator)
	suite.documentClassifier = new(mocks.DocumentClassifier)
	suite.documentDeleter = new(mocks.DocumentDeleter)
	suite.documentGetter = new(mocks.DocumentGetter)
	suite.documentCreator.On("Create", mock.Anything, mock.Anything).Return(suite.document, nil)
	suite.documentClassifier.On("Classify", mock.Anything, mock.Anything,
		mock.Anything, mock.Anything).Return(suite.classificationResult, nil)
	suite.documentDeleter.On("Delete", mock.Anything, mock.Anything).Return(nil)
	suite.documentGetter.On("GetAll", mock.Anything).Return(nil, nil)

	suite.ctx, _ = context.WithCancel(context.Background())

	suite.sut = client.NewFileClassifier(suite.documentCreator, suite.documentClassifier, suite.documentDeleter)
}

func TestFileClassifierSuiteRunner(t *testing.T) {
	suite.Run(t, new(FileClassifierSuite))
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Calls_Create_Document_With_File_Content() {
	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Nil(suite.T(), err)
	suite.documentCreator.AssertCalled(suite.T(), "Create", mock.Anything, suite.testFileContentBuf)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Calls_Create_Document_With_Background_Context() {
	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Nil(suite.T(), err)
	suite.documentCreator.AssertCalled(suite.T(), "Create", context.Background(), mock.Anything)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Calls_Classify_With_DocumentId_ClassifierName_And_Profile() {
	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Nil(suite.T(), err)
	suite.documentClassifier.AssertCalled(suite.T(), "Classify", mock.Anything, suite.documentId, suite.classifierName, suite.readProfileArgs)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Calls_Delete_With_DocumentId() {
	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Nil(suite.T(), err)
	suite.documentDeleter.AssertCalled(suite.T(), "Delete", mock.Anything, suite.documentId)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Calls_Delete_With_Background_Context() {
	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Nil(suite.T(), err)
	suite.documentDeleter.AssertCalled(suite.T(), "Delete", context.Background(), mock.Anything)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Returns_Error_If_CreateDocument_Fails() {
	suite.documentCreator.ExpectedCalls = nil
	classifyErr := errors.New("simulated error")
	suite.documentCreator.On("Create", mock.Anything, mock.Anything).Return(client.Document{}, classifyErr)

	_, err := suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	assert.Equal(suite.T(), classifyErr, err)
}

func (suite *FileClassifierSuite) TestFileClassifier_Classify_Deletes_Document_If_ClassifyDocument_Fails() {
	expectedErr := errors.New("simulated error")
	suite.documentClassifier.ExpectedCalls = nil
	suite.documentClassifier.
		On("Classify", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(nil, expectedErr)

	suite.sut.Classify(suite.ctx, suite.testFileContentBuf, suite.classifierName, suite.readProfileArgs)

	suite.documentDeleter.AssertCalled(suite.T(), "Delete", mock.Anything, suite.documentId)
}
