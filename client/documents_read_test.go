package client_test

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/net/mocks"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"io"
	"io/ioutil"
	"net/http"
	"testing"
)

var exampleReadsHttpResponse *http.Response

type DocumentsClientReadSuite struct {
	suite.Suite
	sut          *client.DocumentsClient
	httpClient   *mocks.HttpDoer
	fileContents []byte
	documentId   string
	profileArgs  client.ReadProfileArgs
	ctx          context.Context
}

func (suite *DocumentsClientReadSuite) SetupTest() {
	exampleReadsHttpResponse = anHttpResponse([]byte(exampleReadsResponseBody))

	suite.httpClient = new(mocks.HttpDoer)
	suite.sut = client.NewDocumentsClient(apiUrl, suite.httpClient)

	suite.fileContents = generators.Bytes()
	suite.documentId = generators.String("documentId")
	suite.profileArgs = client.ReadProfileArgs{
		UseReadProfile: false,
	}

	suite.ctx = context.Background()
}

func TestDocumentsClientReadSuiteRunner(t *testing.T) {
	suite.Run(t, new(DocumentsClientReadSuite))
}

func (suite *DocumentsClientReadSuite) AssertRequestIssued(method string, urlPath string) {
	assert.Equal(suite.T(), method, suite.request().Method)
	assert.Equal(suite.T(), urlPath, suite.request().URL.Path)
}

func (suite *DocumentsClientReadSuite) request() *http.Request {
	require.Len(suite.T(), suite.httpClient.Calls, 1)

	call := suite.httpClient.Calls[0]
	require.Len(suite.T(), call.Arguments, 1)

	return (call.Arguments[0]).(*http.Request)
}

func (suite *DocumentsClientReadSuite) requestHeader(name string) string {
	headerMap := suite.request().Header

	suite.Require().Len(headerMap[name], 1)

	return headerMap[name][0]
}

func (suite *DocumentsClientReadSuite) Test_Correct_Url_Is_Called() {
	suite.httpClient.
		On("Do", mock.Anything).
		Return(exampleReadsHttpResponse, nil)

	suite.sut.Read(suite.ctx, suite.documentId, suite.profileArgs)

	suite.AssertRequestIssued("PUT", apiUrl+"/documents/"+suite.documentId+"/reads")
}

func (suite *DocumentsClientReadSuite) Test_Body_Is_Empty_If_No_Read_Profile_Is_Specified_In_Args() {
	suite.httpClient.
		On("Do", mock.Anything).
		Return(exampleReadsHttpResponse, nil)

	profileArgs := client.ReadProfileArgs{
		UseReadProfile: false,
	}

	suite.sut.Read(suite.ctx, suite.documentId, profileArgs)

	suite.AssertRequestIssuedWith("PUT", apiUrl+"/documents/"+suite.documentId+"/reads", nil, map[string]string{})
}

func (suite *DocumentsClientReadSuite) Test_Body_Specifies_Read_Profile__If_One_Is_Specified_In_Args() {
	suite.httpClient.
		On("Do", mock.Anything).
		Return(exampleReadsHttpResponse, nil)

	profileArgs := client.ReadProfileArgs{
		UseReadProfile: true,
		ProfileName:    generators.String("profile"),
	}

	expectedRequest := &client.ReadDocumentRequest{
		ProfileName: profileArgs.ProfileName,
	}
	bodyBytes, _ := json.Marshal(expectedRequest)
	expectedBody := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	expectedHeaders := map[string]string{
		"Content-Type": "application/json",
	}

	suite.sut.Read(suite.ctx, suite.documentId, profileArgs)

	suite.AssertRequestIssuedWith("PUT", apiUrl+"/documents/"+suite.documentId+"/reads", expectedBody, expectedHeaders)
}

func (suite *DocumentsClientReadSuite) Test_Response_Body_Is_Returned_From_HttpClient() {
	suite.httpClient.
		On("Do", mock.Anything).
		Return(exampleReadsHttpResponse, nil)

	data, _ := suite.sut.ReadResult(suite.ctx, suite.documentId, client.ReadPDF, suite.profileArgs)

	suite.Assert().Equal(exampleReadsHttpResponse.Body, data)
}

func (suite *DocumentsClientReadSuite) Test_Err_Is_Returned_From_HttpClient_When_Requesting_Result() {
	expectedErr := errors.New("request error")
	suite.httpClient.
		On("Do", mock.Anything).
		Return(nil, expectedErr)

	_, receivedErr := suite.sut.ReadResult(suite.ctx, suite.documentId, client.ReadPDF, suite.profileArgs)

	suite.Assert().Equal(expectedErr, receivedErr)
}

func (suite *DocumentsClientReadSuite) Test_Err_Is_Returned_From_HttpClient_When_Performing_Read() {
	expectedErr := errors.New("request error")
	suite.httpClient.
		On("Do", mock.Anything).
		Return(nil, expectedErr)

	receivedErr := suite.sut.Read(suite.ctx, suite.documentId, suite.profileArgs)

	suite.Assert().Equal(expectedErr, receivedErr)
}

func (suite *DocumentsClientReadSuite) Test_Correct_Accept_Header_Is_Set_When_Requesting_Result() {
	var testdata = []struct {
		mode           client.ReadMode
		expectedHeader string
	}{
		{
			mode:           client.ReadPDF,
			expectedHeader: "application/pdf",
		},
		{
			mode:           client.ReadText,
			expectedHeader: "text/plain",
		},
	}

	for _, td := range testdata {
		suite.SetupTest()
		suite.httpClient.
			On("Do", mock.Anything).
			Return(AnHttpResponse(generators.Bytes()), nil)

		suite.sut.ReadResult(suite.ctx, suite.documentId, td.mode, suite.profileArgs)

		sentAcceptHeader := suite.requestHeader("Accept")
		suite.Assert().Equal(td.expectedHeader, sentAcceptHeader)
	}
}

func (suite *DocumentsClientReadSuite) AssertRequestIssuedWith(method string, urlPath string, expectedBody io.Reader, headers map[string]string) {
	suite.AssertRequestIssued(method, urlPath)

	assert.Equal(suite.T(), expectedBody, suite.request().Body)

	for k, expectedValue := range headers {
		actualValue, ok := suite.request().Header[k]

		assert.True(suite.T(), ok)
		assert.Contains(suite.T(), actualValue, expectedValue)
	}
}

var exampleReadsResponseBody = `{
	"_links": {
		"self": {
			"href": "/documents/fdfwscb3SUKKqLc_5Cazhg/reads",
			"method": "PUT"
		},
		"parent": {
			"href": "/documents/fdfwscb3SUKKqLc_5Cazhg"
		}
	}
}`
