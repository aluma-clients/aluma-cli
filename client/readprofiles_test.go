package client_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aluma-clients/aluma-cli/client"
	"gitlab.com/aluma-clients/aluma-cli/net/mocks"
	"gitlab.com/aluma-clients/aluma-cli/test/generators"
	"net/http"
	"testing"
)

type ReadProfilesClientSuite struct {
	suite.Suite
	sut             *client.ReadProfilesClient
	httpClient      *mocks.HttpDoer
	readProfileName string
	languages       []string
}

var exampleGetAllProfilesHttpResponse *http.Response
var exampleCreateReadProfileHttpResponse *http.Response

func (suite *ReadProfilesClientSuite) SetupTest() {
	exampleGetAllProfilesHttpResponse = AnHttpResponse([]byte(exampleGetAllReadProfilesResponse))
	exampleCreateReadProfileHttpResponse = AnHttpResponse([]byte(exampleCreateReadProfileResponse))

	suite.httpClient = new(mocks.HttpDoer)
	suite.readProfileName = generators.String("name")
	suite.languages = []string{generators.String("language"), generators.String("language"), generators.String("language")}
	suite.sut = client.NewReadProfilesClient(apiUrl, suite.httpClient)
}

func TestReadProfilesClientSuiteRunner(t *testing.T) {
	suite.Run(t, new(ReadProfilesClientSuite))
}

func (suite *ReadProfilesClientSuite) request() *http.Request {
	require.Len(suite.T(), suite.httpClient.Calls, 1)

	call := suite.httpClient.Calls[0]
	require.Len(suite.T(), call.Arguments, 1)

	return (call.Arguments[0]).(*http.Request)
}

func (suite *ReadProfilesClientSuite) AssertRequestIssued(method string, urlPath string) {
	assert.Equal(suite.T(), method, suite.request().Method)
	assert.Equal(suite.T(), urlPath, suite.request().URL.Path)
}

func (suite *ReadProfilesClientSuite) AssertRequestHasBody(body []byte) {
	buf := bytes.Buffer{}
	_, err := buf.ReadFrom(suite.request().Body)

	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), body, buf.Bytes())
}

func (suite *ReadProfilesClientSuite) Test_CreateReadProfile_Issues_Create_ReadProfile_Request_With_Languages() {
	suite.httpClient.On("Do", mock.Anything).Return(exampleCreateDocHttpResponse, nil)

	suite.sut.Create(context.Background(), suite.readProfileName, suite.languages)

	suite.AssertRequestIssued("POST", apiUrl+"/read-profiles/"+suite.readProfileName)

	request := &client.CreateReadProfileRequest{
		Languages: suite.languages,
	}

	bodyBytes, err := json.Marshal(&request)
	if err != nil {
		panic(err)
	}
	suite.AssertRequestHasBody(bodyBytes)
}

func (suite *ReadProfilesClientSuite) Test_CreateReadProfile_Returns_ReadProfile() {
	suite.httpClient.On("Do", mock.Anything).Return(exampleCreateReadProfileHttpResponse, nil)

	ReadProfile, err := suite.sut.Create(context.Background(), suite.readProfileName, suite.languages)

	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), suite.readProfileName, ReadProfile.Name)
}

func (suite *ReadProfilesClientSuite) Test_CreateReadProfile_Returns_Error_From_Sender() {
	expectedErr := errors.New("simulated error")

	suite.httpClient.On("Do", mock.Anything).Return(nil, expectedErr)

	ReadProfile, err := suite.sut.Create(context.Background(), suite.readProfileName, suite.languages)
	assert.Equal(suite.T(), "", ReadProfile.Name)
	assert.Equal(suite.T(), expectedErr, err)
}

func (suite *ReadProfilesClientSuite) Test_GetAll_Issues_Get_All_Profiles_Request() {
	suite.httpClient.On("Do", mock.Anything).Return(exampleGetAllProfilesHttpResponse, nil)

	suite.sut.GetAll(context.Background())

	suite.AssertRequestIssued("GET", apiUrl+"/read-profiles")
}

func (suite *ReadProfilesClientSuite) Test_GetAll_Profiles_Returns_List_Of_Profiles() {
	suite.httpClient.On("Do", mock.Anything).Return(exampleGetAllProfilesHttpResponse, nil)

	profiles, _ := suite.sut.GetAll(context.Background())

	assert.Equal(suite.T(), 2, len(profiles))
	assert.Equal(suite.T(), "german", profiles[0].Name)
	assert.Equal(suite.T(), 1, len(profiles[0].Languages))
	assert.Equal(suite.T(), "deu", profiles[0].Languages[0])

	assert.Equal(suite.T(), "german_english", profiles[1].Name)
	assert.Equal(suite.T(), 2, len(profiles[1].Languages))
	assert.Equal(suite.T(), "deu", profiles[1].Languages[0])
	assert.Equal(suite.T(), "eng", profiles[1].Languages[1])
}

func (suite *ReadProfilesClientSuite) Test_GetAll_Profiles_Returns_Error_From_Http_Client() {
	expectedErr := errors.New("expected")
	suite.httpClient.On("Do", mock.Anything).Return(nil, expectedErr)

	_, receivedErr := suite.sut.GetAll(context.Background())

	assert.Equal(suite.T(), expectedErr, receivedErr)
}

func (suite *ReadProfilesClientSuite) Test_GetAll_Profiles_Returns_Error_If_Response_Cannot_Be_Parsed() {
	suite.httpClient.On("Do", mock.Anything).Return(AnHttpResponse([]byte("<invalid-json>")), nil)

	_, receivedErr := suite.sut.GetAll(context.Background())

	assert.NotNil(suite.T(), receivedErr)
}

func (suite *ReadProfilesClientSuite) Test_Delete_Profile_Issues_Delete_Profile_Request() {
	suite.httpClient.On("Do", mock.Anything).Return(nil, nil)

	suite.sut.Delete(context.Background(), suite.readProfileName)

	// Assert
	suite.AssertRequestIssued("DELETE", apiUrl+"/read-profiles/"+suite.readProfileName)
}

func (suite *ReadProfilesClientSuite) Test_Delete_Profile_Returns_Nil_On_Success() {
	suite.httpClient.On("Do", mock.Anything).Return(nil, nil)

	suite.sut.Delete(context.Background(), suite.readProfileName)

	// Assert
	suite.AssertRequestIssued("DELETE", apiUrl+"/read-profiles/"+suite.readProfileName)
}

func (suite *ReadProfilesClientSuite) Test_Delete_Profile_Returns_Error_From_Http_Client() {
	expectedErr := errors.New("an-error")
	suite.httpClient.On("Do", mock.Anything).Return(nil, expectedErr)

	err := suite.sut.Delete(context.Background(), suite.readProfileName)

	// Assert
	assert.Equal(suite.T(), expectedErr, err)
}

var exampleCreateReadProfileResponse = `{
  "name": "german_english",
  "languages": [
    "deu",
    "eng"
  ],
  "_links": {
    "self": {
      "href": "/read-profiles/german_english"
    }
  }
}`

var exampleGetAllReadProfilesResponse = `{
  "read-profiles": [
    {
      "name": "german",
      "languages": [
        "deu"
      ],
      "_links": {
        "self": {
          "href": "/read-profiles/german"
        }
      }
    },
    {
      "name": "german_english",
      "languages": [
        "deu",
		"eng"
      ],
      "_links": {
        "self": {
          "href": "/read-profiles/german_english"
        }
      }
    }
  ]
}`
